# Dana Swift's STM32F103 kernel libaries

## Download

``` shellsession
git clone git@bitbucket.org:uwaploe/ds-stm32.git stm32
```

## Build

``` shellsession
make all
```

If the build is successful, the libraries are installed under `./lib/`.

