#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# $Id: makefile,v 1.1.1.1 2019/04/12 17:25:17 swift Exp $
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# RCS Log:
#
# $Log: makefile,v $
# Revision 1.1.1.1  2019/04/12 17:25:17  swift
# Stm32 HAL library.
#
# Revision 1.1.1.1  2017/06/28 21:39:36  swift
# Stm32f103 SwiftWare RTC model.
#
# Revision 1.1  2017/06/23 14:28:32  swift
# Initial revision
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SHELL = /bin/bash
LN := ln

.DEFAULT:
.SUFFIXES:
.PRECIOUS: 
.PHONY: force

# read definitions of global compilation switches
-include .makefile.defs

# directory definitions
incDir = ./include#
libDir = ./lib#
halDir = ./Drivers/STM32F1xx_HAL_Driver/Src#
fatDir = ./Middlewares/Third_Party/FatFs/src#
srcDir = ./kernel#


# construct a list of all source files for the Stm32 library
Stm32_cFiles := $(wildcard $(halDir)/*.c)

# filter-out the HAL RTC model so that SwiftWare version can be substituted
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_rtc.c, $(Stm32_cFiles))
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_rtc_ex.c, $(Stm32_cFiles))

# filter-out the HAL GPIO model so that SwiftWare version can be substituted
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_gpio.c, $(Stm32_cFiles))
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_gpio_ex.c, $(Stm32_cFiles))

# filter-out the HAL UART model so that SwiftWare version can be substituted
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_uart.c, $(Stm32_cFiles))

# filter-out the HAL SPI model so that SwiftWare version can be substituted
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_spi.c, $(Stm32_cFiles))
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_spi_ex.c, $(Stm32_cFiles))

# filter-out the HAL ADC model so that SwiftWare version can be substituted
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_adc.c, $(Stm32_cFiles))
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_adc_ex.c, $(Stm32_cFiles))

# filter-out the HAL timer model so that SwiftWare version can be substituted
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_tim.c, $(Stm32_cFiles))
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_tim_ex.c, $(Stm32_cFiles))

# filter-out the HAL DMA model so that SwiftWare version can be substituted
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_dma.c, $(Stm32_cFiles))

# filter-out the HAL FLASH model so that SwiftWare version can be substituted
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_flash.c, $(Stm32_cFiles))
Stm32_cFiles := $(filter-out $(halDir)/stm32f1xx_hal_flash_ex.c, $(Stm32_cFiles))

# construct list of object files in the Stm32 library
Stm32_oFiles := $(subst .c,.o, $(Stm32_cFiles))

# construct dependency files in the Stm32 library
Stm32_dFiles := $(subst .c,.d, $(Stm32_cFiles))

# construct a list of header files used by the Stm32 library
Stm32_hFiles := $(notdir $(shell find ./Drivers -name \*.h))


# construct a list of all source files for the FatFs library
FatFs_cFiles := $(shell find $(fatDir) -name \*.c)

# construct list of object files in the FatFs library
FatFs_oFiles := $(subst .c,.o, $(FatFs_cFiles))

# construct dependency files in the FatFs library
FatFs_dFiles := $(subst .c,.d, $(FatFs_cFiles))

# construct a list of header files used by the FatFs library
FatFs_hFiles := $(notdir $(shell find $(fatDir) -name \*.h))


# construct a list of all source files for the Kernel library
Kernel_cFiles := $(wildcard $(srcDir)/*.c)

# construct list of object files in the Kernel library
Kernel_oFiles := $(subst .c,.o, $(Kernel_cFiles))

# construct dependency files in the Kernel library
Kernel_dFiles := $(subst .c,.d, $(Kernel_cFiles))

# construct a list of header files used by the Kernel library
Kernel_hFiles := $(notdir $(shell find ./Inc -name \*.h))


# default target
all: headers libstm32.a libkernel.a
	@echo Done making $@ ...

# clean both Stm32, FatFs, and Kernel library components
clean: clean-headers clean-stm32 clean-kernel clean-fatfs
	@echo Done making $@ ...

# clean the Stm32 library components
clean-stm32: force
	-rm -f $(Stm32_oFiles) $(Stm32_dFiles) $(libDir)/libstm32.a
	@echo Done making $@ ...

# clean the Stm32 library components
clean-fatfs: force
	-rm -f $(FatFs_oFiles) $(FatFs_dFiles) $(libDir)/libfatfs.a
	@echo Done making $@ ...

# clean the Kernel library components
clean-kernel: force
	-rm -f $(Kernel_oFiles) $(Kernel_dFiles) $(libDir)/libkernel.a
	-rm -f SW4STM32/startup_stm32f103xg.o
	@echo Done making $@ ...

# clean both Stm32 and Kernel library components and remove the lib directory
realclean: clean
	-rm -rf $(libDir) $(incDir) 
	@echo Done making $@ ...

# debugging target that lists components of both Stm32, FatFs, and Kernel libraries
test: force
	@echo libDir = $(libDir)
	@echo 
	@echo Stm32_cFiles = $(Stm32_cFiles)
	@echo Stm32_oFiles = $(Stm32_oFiles)
	@echo Stm32_dFiles = $(Stm32_dFiles)
	@echo Stm32_hFiles = $(Stm32_hFiles)
	@echo 
	@echo FatFs_cFiles = $(FatFs_cFiles)
	@echo FatFs_oFiles = $(FatFs_oFiles)
	@echo FatFs_dFiles = $(FatFs_dFiles)
	@echo FatFs_hFiles = $(FatFs_hFiles)
	@echo 
	@echo Kernel_cFiles = $(Kernel_cFiles)
	@echo Kernel_oFiles = $(Kernel_oFiles)
	@echo Kernel_dFiles = $(Kernel_dFiles)
	@echo Kernel_hFiles = $(Kernel_hFiles)

# assemble the pre-main() startup code
SW4STM32/%.o: SW4STM32/%.s 
	arm-none-eabi-as $(ARMFLAGS) $(DBG) -o $@ $<

# compile one component of the Kernel library
$(srcDir)/%.o: $(srcDir)/%.c
	-@rm -f $@
	arm-none-eabi-gcc $(ARMFLAGS) \
      $(STM32FLAGS) -DSTM32F103xG -DUSE_STM3210E_EVAL -I$(fatDir){,/drivers} \
      -I./Drivers/CMSIS/{,Device/ST/STM32F1xx/}Include -I./Drivers/STM32F1xx_HAL_Driver/Inc -I./Inc\
      $(CFLAGS) $(DBG) \
      -MMD -MP -MF"$(subst .o,.d,$@)" -MT"$@" -o $@ $<

# list the dependencies of the Kernel library
libkernel.a: $(libDir) $(libDir)/libkernel.a
	@echo Done making $@ ...

# create the Kernel library from object components
$(libDir)/libkernel.a: $(Kernel_oFiles) SW4STM32/startup_stm32f103xg.o
	arm-none-eabi-ar rvu $@ $?
	arm-none-eabi-ranlib $@
	@echo Done making $@ ...


# compile one component of the Stm32 library
$(halDir)/%.o: $(halDir)/%.c
	-@rm -f $@
	arm-none-eabi-gcc $(ARMFLAGS) \
      $(STM32FLAGS) -DUSE_HAL_DRIVER -DSTM32F103xG \
      -I./Drivers/CMSIS/{,Device/ST/STM32F1xx/}Include -I./Drivers/STM32F1xx_HAL_Driver/Inc -I./Inc \
      $(CFLAGS) -MMD -MP -MF"$(subst .o,.d,$@)" \
      -MT"$@" \
      -o $@ $<

# list the dependencies of the Stm32 library
libstm32.a: $(libDir) $(libDir)/libstm32.a
	@echo Done making $@ ...

# create the Stm32 library from object components
$(libDir)/libstm32.a: $(Stm32_oFiles)
	arm-none-eabi-ar rvu $@ $?
	arm-none-eabi-ranlib $@
	@echo Done making $@ ...


# compile one component of the FatFs library
$(fatDir)/%.o: $(fatDir)/%.c
	-@rm -f $@
	arm-none-eabi-gcc $(ARMFLAGS) \
      $(STM32FLAGS) -DUSE_HAL_DRIVER -DSTM32F103xG \
      -I./Inc -I./Drivers/STM32F1xx_HAL_Driver/Inc -I./Drivers/CMSIS/{,Device/ST/STM32F1xx/}Include \
      -I$(fatDir) -I./$(@D) \
      $(CFLAGS) $(DBG) -MMD -MP -MF"$(subst .o,.d,$@)" \
      -MT"$@" \
      -o $@ $<

# list the dependencies of the FatFs library
libfatfs.a: $(libDir) $(libDir)/libfatfs.a
	@echo Done making $@ ...

# create the FatFs library from object components
$(libDir)/libfatfs.a: $(FatFs_oFiles)
	arm-none-eabi-ar rvu $@ $?
	arm-none-eabi-ranlib $@
	@echo Done making $@ ...

# create the directory to contain the Stm32, FatFs, and Kernel libraries
$(libDir):
	mkdir $@


# manage creation of symbolic links to header files
headers: $(incDir) $(incDir)/Legacy \
	      $(addprefix $(incDir)/, $(Stm32_hFiles)) \
	      $(addprefix $(incDir)/, $(FatFs_hFiles)) \
	      $(addprefix $(incDir)/, $(Kernel_hFiles))
	@echo Done making $@ ...

# clean the header files from the include directory
clean-headers: force
	-rm -f $(incDir)/Legacy \
          $(addprefix $(incDir)/, $(Stm32_hFiles)) \
	       $(addprefix $(incDir)/, $(FatFs_hFiles)) \
	       $(addprefix $(incDir)/, $(Kernel_hFiles))
	@echo Done making $@ ...

# create the include directory
$(incDir):
	mkdir $@

# create a symbolic link to the legacy directory
$(incDir)/Legacy: 
	cd $(incDir); $(LN) -s . Legacy

# pattern-target for include files
$(incDir)/%.h: ./Inc/%.h
	$(LN) -st $(incDir) ../$<

# pattern-target for include files
$(incDir)/%.h: ./Drivers/CMSIS/Device/ST/STM32F1xx/Include/%.h
	$(LN) -st $(incDir) ../$<

# pattern-target for include files
$(incDir)/%.h: ./Drivers/CMSIS/Include/%.h
	$(LN) -st $(incDir) ../$<

# pattern-target for include files
$(incDir)/%.h: ./Drivers/STM32F1xx_HAL_Driver/Inc/%.h
	$(LN) -st $(incDir) ../$<

# pattern-target for include files
$(incDir)/%.h: ./Drivers/STM32F1xx_HAL_Driver/Inc/Legacy/%.h
	$(LN) -st $(incDir) ../$<

# pattern-target for include files
$(incDir)/%.h: ./Middlewares/Third_Party/FatFs/src/%.h
	$(LN) -st $(incDir) ../$<

# pattern-target for include files
$(incDir)/%.h: ./Middlewares/Third_Party/FatFs/src/drivers/%.h
	$(LN) -st $(incDir) ../$<

STM32F103ZGTx_FLASH.ld:
	$(LN) -s SW4STM32/stm32/$@
