/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id: main.c,v 1.1.1.1 2019/04/12 17:25:17 swift Exp $
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log: main.c,v $
 * Revision 1.1.1.1  2019/04/12 17:25:17  swift
 * Stm32 HAL library.
 *
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define mainChangeLog "$RCSfile: main.c,v $  $Revision: 1.1.1.1 $   $Date: 2019/04/12 17:25:17 $"

#include <stm32f1xx_hal.h>

extern unsigned char _estack[];
extern unsigned char _Min_Stack_Size[];
extern unsigned long int Apf11HeapTop;

static volatile uint32_t STACK_SIZE = 0U;
static uint32_t STACK_CANARY_SIZE = 32U;

/* declare functions with external linkage */
void     Apf11Init(void);
int      apf11main(void);
void     BusFault_Handler(void);
void     DebugMon_Handler(void);
void     ErrorHandler(const char *FuncName);
void     HAL_MspInit(void);
void     HAL_MPU_Init();
void     HAL_NVIC_SystemReset(void);
void     HardFault_Handler(void);
long int LogEntry(const char *function_name, const char *format, ...);
void     MemManage_Handler(void);
void     NMI_Handler(void);
void     PendSV_Handler(void);
void     RTC_IRQHandler(void);
void     SVC_Handler(void);
void     SystemClock_Config(void);
void     SysTick_Handler(void);
void     UsageFault_Handler(void);
void     USART1_IRQHandler(void);
void     USART2_IRQHandler(void);
void     USART3_IRQHandler(void);

/*------------------------------------------------------------------------*/
/* entry point for the reset vector                                       */
/*------------------------------------------------------------------------*/
/**
   This function is the entry point for the reset vector.
*/
int main(void)
{
   /* define the logging signature */
   static const char *FuncName="main()";

   /* initialize the HAL module */
   HAL_Init();

   /* configure the RCC module */
   SystemClock_Config();

   /* low-level Apf11 initialization */
   Apf11Init();

   /* transfer to higher level control */
   apf11main();

   /* make a logentry of the kernel panic */
   LogEntry(FuncName,"Kernel panic: apf11main() terminated "
            "unexpectedly; initiating a reset of the Apf11.\n");

   /* initiate a software reset of the STM32 */
   HAL_NVIC_SystemReset();

   return 0;
}

/*------------------------------------------------------------------------*/
/* function to log failures of assert_param()                             */
/*------------------------------------------------------------------------*/
/**
   This function creates engineering logentries resulting from
   failures of assert_param() in the HAL library.
*/
void assert_failed(uint8_t* file, uint32_t line)
{
   /* function name for log entries */
   static const char *FuncName="assert_failed()";

   /* note the failure in the engineering logs */
   LogEntry(FuncName,"Assertion failed: %s:%ld\n",file,line);
}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void BusFault_Handler(void)
{
   /* define the logging signature */
   static const char *FuncName="BusFault_Handler()";

   /* execute a system reset */
   ErrorHandler(FuncName);
}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void DebugMon_Handler(void)
{
   /* define the logging signature */
   static const char *FuncName="DebugMon_Handler()";

   /* execute a system reset */
   ErrorHandler(FuncName);
}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void ErrorHandler(const char *FuncName)
{
   /* make an engineering logentry */
   LogEntry(FuncName,"Exception encountered; "
            "initiating a reset of the STM32.\n");

   /* initiate a reset of the STM32 */
   HAL_NVIC_SystemReset();
}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
void HAL_MspInit(void)
{
   __HAL_RCC_AFIO_CLK_ENABLE();

   HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_1);

   /* System interrupt init*/
   /* MemoryManagement_IRQn interrupt configuration */
   HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
   /* BusFault_IRQn interrupt configuration */
   HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
   /* UsageFault_IRQn interrupt configuration */
   HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
   /* SVCall_IRQn interrupt configuration */
   HAL_NVIC_SetPriority(SVCall_IRQn, 0, 0);
   /* DebugMonitor_IRQn interrupt configuration */
   HAL_NVIC_SetPriority(DebugMonitor_IRQn, 0, 0);
   /* PendSV_IRQn interrupt configuration */
   HAL_NVIC_SetPriority(PendSV_IRQn, 0, 0);
   /* SysTick_IRQn interrupt configuration */
   HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

   /**ENABLE: Full SWJ (JTAG-DP + SW-DP): Reset State */
   __HAL_AFIO_REMAP_SWJ_ENABLE();
}

void HAL_MPU_Init()
{
    MPU_Region_InitTypeDef MPU_InitStruct;

    // Make sure we remember the address
    if (STACK_SIZE == 0U)
    {
        STACK_SIZE = (uint32_t)_Min_Stack_Size;
    }

    uint32_t stackLowAddress = (uint32_t)_estack - STACK_SIZE;

    HAL_MPU_Disable();

    LogEntry("HAL_MPU_Init()", "Setting Stack Canary at address: 0x%08x\n", stackLowAddress);

    MPU_InitStruct.Enable = MPU_REGION_ENABLE;
    MPU_InitStruct.BaseAddress = stackLowAddress;
    MPU_InitStruct.Size        = MPU_REGION_SIZE_32B;
    MPU_InitStruct.AccessPermission = MPU_REGION_NO_ACCESS;
    MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
    MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
    MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
    MPU_InitStruct.Number = MPU_REGION_NUMBER0;
    MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
    MPU_InitStruct.SubRegionDisable = 0x00;
    MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

    HAL_MPU_ConfigRegion(&MPU_InitStruct);

    HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void HardFault_Handler(void)
{
    /* define the logging signature */
    static const char *FuncName="HardFault_Handler()";

    /* execute a system reset */
    ErrorHandler(FuncName);
}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void MemManage_Handler(void)
{
    /* define the logging signature */
    static const char *FuncName="MemManage_Handler()";

    uint32_t addr = SCB->MMFAR;
    uint32_t stackLowAddress = (uint32_t)_estack - STACK_SIZE;
    uint32_t diff = addr > stackLowAddress ? addr - stackLowAddress : stackLowAddress - addr;
    LogEntry(
        FuncName,
        "Memory Fault at address: %08x. Offset from stack canary: %08x\n",
        addr,
        diff);
    if (diff <= STACK_CANARY_SIZE)
    {
        LogEntry(
            FuncName,
            "Likely Stack Overflow at address: %08x. Stack bottom is: %08x\n!",
            addr,
            stackLowAddress);

        uint32_t newStackLow = (uint32_t)_estack - (2 * STACK_SIZE);
        if (newStackLow > Apf11HeapTop)
        {
            STACK_SIZE *= 2;
            HAL_MPU_Init();
            return;
        }

        LogEntry(FuncName, "Not enough space to grow the stack, resetting!");
    }

    /* execute a system reset */
    ErrorHandler(FuncName);
}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void NMI_Handler(void) {HAL_RCC_NMI_IRQHandler();}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void PendSV_Handler(void)
{
   /* define the logging signature */
   static const char *FuncName="PendSV_Handler()";

   /* execute a system reset */
   ErrorHandler(FuncName);
}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void RTC_IRQHandler(void)
{
   /* prototype for function to return RTC handle */
   RTC_HandleTypeDef *HAL_RTC_HandleGet(void);

   /* call the RTC IRQ handler */
   HAL_RTCEx_RTCIRQHandler(HAL_RTC_HandleGet());
}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void SVC_Handler(void)
{
   /* define the logging signature */
   static const char *FuncName="SVC_Handler()";

   /* execute a system reset */
   ErrorHandler(FuncName);
}

/*------------------------------------------------------------------------*/
/* function to configure the Stm32 RCC peripheral                         */
/*------------------------------------------------------------------------*/
void SystemClock_Config(void)
{
   /* define the logging signature */
   static const char *FuncName="SystemClock_Config()";

   /* define local work objects */
   RCC_OscInitTypeDef RCC_OscInitStruct;
   RCC_ClkInitTypeDef RCC_ClkInitStruct;
   RCC_PeriphCLKInitTypeDef PeriphClkInit;

   /* initialize the structures to control CPU, AHB and APB bus oscillators */
   RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
   RCC_OscInitStruct.HSEState = RCC_HSE_ON;
   RCC_OscInitStruct.LSEState = RCC_LSE_ON;
   RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;

   /* initialize the CPU, AHB and APB bus oscillators */
   if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {ErrorHandler(FuncName);}

   /* initialize structures to control the CPU, AHB and APB bus clocks */
   RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK|
                                 RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
   RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSE;
   RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
   RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
   RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
   
   /* initialize the CPU, AHB and APB bus clocks */
   if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK) {ErrorHandler(FuncName);}

   /* initialize structures used to control peripherals  */
   PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_ADC;
   PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
   PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV2;

   /* initialize peripheral clocks */
   if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {ErrorHandler(FuncName);}

   /* enables the Clock Security System */
   HAL_RCC_EnableCSS();

   /* configure the systick interrupt time */
   HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

   /* configure the systick */
   HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

   /* SysTick_IRQn interrupt configuration */
   HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void SysTick_Handler(void) {HAL_IncTick(); HAL_SYSTICK_IRQHandler();}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void UsageFault_Handler(void)
{
   /* define the logging signature */
   static const char *FuncName="UsageFault_Handler()";

   /* execute a system reset */
   ErrorHandler(FuncName);
}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void USART1_IRQHandler(void)
{
   /* prototype for SwiftWare UART irq handler */
   int Stm32UartIrqHandler(void);

   /* call the SwiftWare UART irq handler */
   Stm32UartIrqHandler();
}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void USART2_IRQHandler(void)
{
   /* prototype for SwiftWare CTD UART irq handler */
   int CtdIrqHandler(void);

   /* call the SwiftWare CTD UART irq handler */
   CtdIrqHandler();
}

/*------------------------------------------------------------------------*/
/* error handler defined in startup_stm32f103xg.s                         */
/*------------------------------------------------------------------------*/
void USART3_IRQHandler(void)
{
   /* prototype for SwiftWare current loop UART irq handler */
   int ConioIrqHandler(void);

   /* call the SwiftWare CTD UART irq handler */
   ConioIrqHandler();
}
