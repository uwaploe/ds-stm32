/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
#ifndef STM32F103GPIO_H
/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define EXP_GPIO_SPI_CS_Pin GPIO_PIN_2
#define EXP_GPIO_SPI_CS_GPIO_Port GPIOE
#define A19_Pin GPIO_PIN_3
#define A19_GPIO_Port GPIOE
#define EXP_DUART3_SPI_CS_Pin GPIO_PIN_4
#define EXP_DUART3_SPI_CS_GPIO_Port GPIOE
#define EXP_DUART2_SPI_CS_Pin GPIO_PIN_5
#define EXP_DUART2_SPI_CS_GPIO_Port GPIOE
#define EXP_DUART1_SPI_CS_Pin GPIO_PIN_6
#define EXP_DUART1_SPI_CS_GPIO_Port GPIOE
#define TAMPER_Pin GPIO_PIN_13
#define TAMPER_GPIO_Port GPIOC
#define RTC_32KHZ_Pin GPIO_PIN_14
#define RTC_32KHZ_GPIO_Port GPIOC
#define RTC_NC_Pin GPIO_PIN_15
#define RTC_NC_GPIO_Port GPIOC
#define A0_Pin GPIO_PIN_0
#define A0_GPIO_Port GPIOF
#define A1_Pin GPIO_PIN_1
#define A1_GPIO_Port GPIOF
#define A2_Pin GPIO_PIN_2
#define A2_GPIO_Port GPIOF
#define A3_Pin GPIO_PIN_3
#define A3_GPIO_Port GPIOF
#define A4_Pin GPIO_PIN_4
#define A4_GPIO_Port GPIOF
#define A5_Pin GPIO_PIN_5
#define A5_GPIO_Port GPIOF
#define ADC3_IN4_Pin GPIO_PIN_6
#define ADC3_IN4_GPIO_Port GPIOF
#define ADC3_IN5_Pin GPIO_PIN_7
#define ADC3_IN5_GPIO_Port GPIOF
#define ADC3_IN6_Pin GPIO_PIN_8
#define ADC3_IN6_GPIO_Port GPIOF
#define ADC3_IN7_Pin GPIO_PIN_9
#define ADC3_IN7_GPIO_Port GPIOF
#define ADC3_IN8_Pin GPIO_PIN_10
#define ADC3_IN8_GPIO_Port GPIOF
#define ADC123_IN10_Pin GPIO_PIN_0
#define ADC123_IN10_GPIO_Port GPIOC
#define ADC123_IN11_Pin GPIO_PIN_1
#define ADC123_IN11_GPIO_Port GPIOC
#define ADC123_IN12_Pin GPIO_PIN_2
#define ADC123_IN12_GPIO_Port GPIOC
#define ADC123_IN13_Pin GPIO_PIN_3
#define ADC123_IN13_GPIO_Port GPIOC
#define WAKE_Pin GPIO_PIN_0
#define WAKE_GPIO_Port GPIOA
#define EXP_DUART4_SPI_CS_Pin GPIO_PIN_1
#define EXP_DUART4_SPI_CS_GPIO_Port GPIOA
#define UART2_TXD_Pin GPIO_PIN_2
#define UART2_TXD_GPIO_Port GPIOA
#define UART2_RXD_Pin GPIO_PIN_3
#define UART2_RXD_GPIO_Port GPIOA
#define CTD_PWR_CTL_Pin GPIO_PIN_4
#define CTD_PWR_CTL_GPIO_Port GPIOA
#define SPI1_SCK_Pin GPIO_PIN_5
#define SPI1_SCK_GPIO_Port GPIOA
#define SPI1_MISO_Pin GPIO_PIN_6
#define SPI1_MISO_GPIO_Port GPIOA
#define SPI1_MOSI_Pin GPIO_PIN_7
#define SPI1_MOSI_GPIO_Port GPIOA
#define ADC12_IN14_Pin GPIO_PIN_4
#define ADC12_IN14_GPIO_Port GPIOC
#define ADC12_IN15_Pin GPIO_PIN_5
#define ADC12_IN15_GPIO_Port GPIOC
#define ADC12_IN8_Pin GPIO_PIN_0
#define ADC12_IN8_GPIO_Port GPIOB
#define ADC12_IN9_Pin GPIO_PIN_1
#define ADC12_IN9_GPIO_Port GPIOB
#define SD_PWR_EN_Pin GPIO_PIN_2
#define SD_PWR_EN_GPIO_Port GPIOB
#define ACTIVE_ENABLE_3P3V_Pin GPIO_PIN_11
#define ACTIVE_ENABLE_3P3V_GPIO_Port GPIOF
#define A6_Pin GPIO_PIN_12
#define A6_GPIO_Port GPIOF
#define A7_Pin GPIO_PIN_13
#define A7_GPIO_Port GPIOF
#define A8_Pin GPIO_PIN_14
#define A8_GPIO_Port GPIOF
#define A9_Pin GPIO_PIN_15
#define A9_GPIO_Port GPIOF
#define A10_Pin GPIO_PIN_0
#define A10_GPIO_Port GPIOG
#define A11_Pin GPIO_PIN_1
#define A11_GPIO_Port GPIOG
#define D4_Pin GPIO_PIN_7
#define D4_GPIO_Port GPIOE
#define D5_Pin GPIO_PIN_8
#define D5_GPIO_Port GPIOE
#define D6_Pin GPIO_PIN_9
#define D6_GPIO_Port GPIOE
#define D7_Pin GPIO_PIN_10
#define D7_GPIO_Port GPIOE
#define D8_Pin GPIO_PIN_11
#define D8_GPIO_Port GPIOE
#define D9_Pin GPIO_PIN_12
#define D9_GPIO_Port GPIOE
#define D10_Pin GPIO_PIN_13
#define D10_GPIO_Port GPIOE
#define D11_Pin GPIO_PIN_14
#define D11_GPIO_Port GPIOE
#define D12_Pin GPIO_PIN_15
#define D12_GPIO_Port GPIOE
#define UART3_TXD_Pin GPIO_PIN_10
#define UART3_TXD_GPIO_Port GPIOB
#define UART3_RXD_Pin GPIO_PIN_11
#define UART3_RXD_GPIO_Port GPIOB
#define RF_VBAT_PWR_EN_Pin GPIO_PIN_12
#define RF_VBAT_PWR_EN_GPIO_Port GPIOB
#define SPI2_SCK_Pin GPIO_PIN_13
#define SPI2_SCK_GPIO_Port GPIOB
#define SPI2_MISO_Pin GPIO_PIN_14
#define SPI2_MISO_GPIO_Port GPIOB
#define SPI2_MOSI_Pin GPIO_PIN_15
#define SPI2_MOSI_GPIO_Port GPIOB
#define D13_Pin GPIO_PIN_8
#define D13_GPIO_Port GPIOD
#define D14_Pin GPIO_PIN_9
#define D14_GPIO_Port GPIOD
#define D15_Pin GPIO_PIN_10
#define D15_GPIO_Port GPIOD
#define A16_Pin GPIO_PIN_11
#define A16_GPIO_Port GPIOD
#define A17_Pin GPIO_PIN_12
#define A17_GPIO_Port GPIOD
#define A18_Pin GPIO_PIN_13
#define A18_GPIO_Port GPIOD
#define D0_Pin GPIO_PIN_14
#define D0_GPIO_Port GPIOD
#define D1_Pin GPIO_PIN_15
#define D1_GPIO_Port GPIOD
#define A12_Pin GPIO_PIN_2
#define A12_GPIO_Port GPIOG
#define A13_Pin GPIO_PIN_3
#define A13_GPIO_Port GPIOG
#define A14_Pin GPIO_PIN_4
#define A14_GPIO_Port GPIOG
#define A15_Pin GPIO_PIN_5
#define A15_GPIO_Port GPIOG
#define SPARE_SENSOR_PWR_EN_Pin GPIO_PIN_6
#define SPARE_SENSOR_PWR_EN_GPIO_Port GPIOG
#define SD_DETECT_Pin GPIO_PIN_7
#define SD_DETECT_GPIO_Port GPIOG
#define EXP_3P3V_PWR_EN_Pin GPIO_PIN_8
#define EXP_3P3V_PWR_EN_GPIO_Port GPIOG
#define PPS_Pin GPIO_PIN_6
#define PPS_GPIO_Port GPIOC
#define CTD_PTS_FP_Pin GPIO_PIN_7
#define CTD_PTS_FP_GPIO_Port GPIOC
#define SDIO_D0_Pin GPIO_PIN_8
#define SDIO_D0_GPIO_Port GPIOC
#define SDIO_D1_Pin GPIO_PIN_9
#define SDIO_D1_GPIO_Port GPIOC
#define COULOMB_OWD_Pin GPIO_PIN_8
#define COULOMB_OWD_GPIO_Port GPIOA
#define UART1_TXD_Pin GPIO_PIN_9
#define UART1_TXD_GPIO_Port GPIOA
#define UART1_RXD_Pin GPIO_PIN_10
#define UART1_RXD_GPIO_Port GPIOA
#define USB_DM_Pin GPIO_PIN_11
#define USB_DM_GPIO_Port GPIOA
#define USB_DP_Pin GPIO_PIN_12
#define USB_DP_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define TDI_Pin GPIO_PIN_15
#define TDI_GPIO_Port GPIOA
#define SDIO_D2_Pin GPIO_PIN_10
#define SDIO_D2_GPIO_Port GPIOC
#define SDIO_D3_Pin GPIO_PIN_11
#define SDIO_D3_GPIO_Port GPIOC
#define SDIO_CLK_Pin GPIO_PIN_12
#define SDIO_CLK_GPIO_Port GPIOC
#define D2_Pin GPIO_PIN_0
#define D2_GPIO_Port GPIOD
#define D3_Pin GPIO_PIN_1
#define D3_GPIO_Port GPIOD
#define SDIO_CMD_Pin GPIO_PIN_2
#define SDIO_CMD_GPIO_Port GPIOD
#define CL_PWR_EN_Pin GPIO_PIN_3
#define CL_PWR_EN_GPIO_Port GPIOD
#define OE_N_Pin GPIO_PIN_4
#define OE_N_GPIO_Port GPIOD
#define WE_N_Pin GPIO_PIN_5
#define WE_N_GPIO_Port GPIOD
#define EEPROM_CS_Pin GPIO_PIN_6
#define EEPROM_CS_GPIO_Port GPIOD
#define CS_N_Pin GPIO_PIN_7
#define CS_N_GPIO_Port GPIOD
#define EXP_VBAT_PWR_EN_Pin GPIO_PIN_9
#define EXP_VBAT_PWR_EN_GPIO_Port GPIOG
#define SPARE_DUART_IRQ_N_Pin GPIO_PIN_10
#define SPARE_DUART_IRQ_N_GPIO_Port GPIOG
#define RF_DUART_IRQ_N_Pin GPIO_PIN_11
#define RF_DUART_IRQ_N_GPIO_Port GPIOG
#define EXP_DUART1_IRQ_N_Pin GPIO_PIN_12
#define EXP_DUART1_IRQ_N_GPIO_Port GPIOG
#define EXP_DUART2_IRQ_N_Pin GPIO_PIN_13
#define EXP_DUART2_IRQ_N_GPIO_Port GPIOG
#define EXP_DUART3_IRQ_N_Pin GPIO_PIN_14
#define EXP_DUART3_IRQ_N_GPIO_Port GPIOG
#define EXP_DUART4_IRQ_N_Pin GPIO_PIN_15
#define EXP_DUART4_IRQ_N_GPIO_Port GPIOG
#define TDO_Pin GPIO_PIN_3
#define TDO_GPIO_Port GPIOB
#define TRST_Pin GPIO_PIN_4
#define TRST_GPIO_Port GPIOB
#define SPARE_GPIO_SPI_CS_Pin GPIO_PIN_5
#define SPARE_GPIO_SPI_CS_GPIO_Port GPIOB
#define SPARE_DUART_SPI_CS_Pin GPIO_PIN_6
#define SPARE_DUART_SPI_CS_GPIO_Port GPIOB
#define RF_DUART_SPI_CS_Pin GPIO_PIN_7
#define RF_DUART_SPI_CS_GPIO_Port GPIOB
#define RF_GPIO_SPI_CS_Pin GPIO_PIN_8
#define RF_GPIO_SPI_CS_GPIO_Port GPIOB
#define RF_3P3V_PWR_EN_Pin GPIO_PIN_9
#define RF_3P3V_PWR_EN_GPIO_Port GPIOB
#define BE_LO_N_Pin GPIO_PIN_0
#define BE_LO_N_GPIO_Port GPIOE
#define BE_HI_N_Pin GPIO_PIN_1
#define BE_HI_N_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */
#endif /* STM32F103GPIO_H */
/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
